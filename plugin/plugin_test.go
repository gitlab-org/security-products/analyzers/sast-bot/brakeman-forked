package plugin

import (
	"os"
	"testing"
)

func TestMatch(t *testing.T) {
	var tcs = []struct {
		Path string
		Want bool
	}{
		{"fixtures/rails5/Gemfile", true},
		{"fixtures/rails5/Gemfile.lock", false},
		{"fixtures/rails5/app/controllers/application_controller.rb", true},
		{"fixtures/simple_library/hello_world.rb", true},
	}

	for _, tc := range tcs {
		fi, err := os.Stat(tc.Path)
		if err != nil {
			t.Error(err)
			continue
		}
		got, err := Match(tc.Path, fi)
		if err != nil {
			t.Error(err)
			continue
		}
		if got != tc.Want {
			t.Errorf("Wrong result for %s: expecting %v but got %v", tc.Path, tc.Want, got)
		}
	}
}
